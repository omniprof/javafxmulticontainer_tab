package com.kenfogel.javafxmulticontainerv4_tab.view;

import com.kenfogel.fishfxhtml.view.FishFXHTMLControllerDND;
import com.kenfogel.fishfxtable.view.FishFXTableController;
import com.kenfogel.fishfxtree.view.FishFXTreeControllerDND;
import com.kenfogel.javafxmulticontainerv4_tab.persistence.FishDAO;
import com.kenfogel.javafxmulticontainerv4_tab.persistence.FishDAOImpl;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the root layout. All of the other layouts are added in code here.
 * This allows us to use the standalone containers with minimal changes.
 *
 * i18n added
 *
 * Added proper logging
 *
 * Found a bug from SceneBuilder that expected an ActionEvent for pressing tabs
 * when it should be Event
 *
 * @author Ken Fogel
 * @version 1.3
 *
 */
public class RootLayoutTabController {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(RootLayoutTabController.class);

    @FXML
    private ResourceBundle resources;

    @FXML // fx:id="tab_1"
    private Tab tab_1; // Value injected by FXMLLoader

    @FXML // fx:id="tab_2"
    private Tab tab_2; // Value injected by FXMLLoader

    @FXML // fx:id="tab_3"
    private Tab tab_3; // Value injected by FXMLLoader

    @FXML // fx:id="tab_4"
    private Tab tab_4; // Value injected by FXMLLoader

    private final FishDAO fishDAO;
    private FishFXTreeControllerDND fishFXTreeController;
    private FishFXTableController fishFXTableController;
    //private FishFXWebViewController fishFXWebViewController;
    private FishFXHTMLControllerDND fishFXHTMLController;

    public RootLayoutTabController() {
        fishDAO = new FishDAOImpl();
    }

    @FXML
    void arriveHTML(Event event) {
        LOG.debug("Web HTML <<<<<<<<<<<<<<<");

    }

    @FXML
    void arriveTable(Event event) {
        LOG.debug("Table View <<<<<<<<<<<<<<<");

    }

    @FXML
    void arriveTree(Event event) {
        LOG.debug("Tree View <<<<<<<<<<<<<<<");

    }

    @FXML
    void arriveWeb(Event event) {
        LOG.debug("Web View <<<<<<<<<<<<<<<");
    }

    /**
     * Here we call upon the methods that load the other containers and then
     * send the appropriate action command to each container
     */
    @FXML
    private void initialize() {

        initTab1Layout();
        initTab2Layout();
        initTab3Layout();
        initTab4Layout();

        // Tell the tree about the table
        setTableControllerToTree();

        try {
            fishFXTreeController.displayTree();
            fishFXTableController.displayTheTable();
            fishFXHTMLController.displayFishAsHTML();
        } catch (SQLException ex) {
            LOG.error("initialize error", ex);
            errorAlert("initialize()");
            Platform.exit();
        }
    }

    /**
     * Send the reference to the FishFXTableController to the
     * FishFXTreeController
     */
    private void setTableControllerToTree() {
        fishFXTreeController.setTableController(fishFXTableController);
    }

    /**
     * The TreeView Layout
     */
    private void initTab1Layout() {
        LOG.debug("Tab 1");
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(RootLayoutTabController.class
                    .getResource("/fxml/FishFXTreeLayout.fxml"));
            AnchorPane treeView = (AnchorPane) loader.load();

            // Give the controller the data object.
            fishFXTreeController = loader.getController();
            fishFXTreeController.setFishDAO(fishDAO);

            tab_1.setContent(treeView);
        } catch (IOException ex) {
            LOG.error("initUpperLeftLayout error", ex);
            errorAlert("initUpperLeftLayout()");
            Platform.exit();
        }
    }

    /**
     * The TableView Layout
     */
    private void initTab2Layout() {
        LOG.debug("Tab 2");
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(RootLayoutTabController.class
                    .getResource("/fxml/FishFXTableLayout.fxml"));
            AnchorPane tableView = (AnchorPane) loader.load();

            // Give the controller the data object.
            fishFXTableController = loader.getController();
            fishFXTableController.setFishDAO(fishDAO);

            tab_2.setContent(tableView);
        } catch (SQLException | IOException ex) {
            LOG.error("initUpperRightLayout error", ex);
            errorAlert("initUpperRightLayout()");
            Platform.exit();
        }
    }

    /**
     * The WebView Layout
     */
    private void initTab3Layout() {
        LOG.debug("Tab 3");
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(RootLayoutTabController.class
                    .getResource("/fxml/FishFXWebViewLayout.fxml"));
            AnchorPane webView = (AnchorPane) loader.load();

            // Retrieve the controller if you must send it messages
            //fishFXWebViewController = loader.getController();
            tab_3.setContent(webView);
        } catch (IOException ex) {
            LOG.error("initLowerLeftLayout error", ex);
            errorAlert("initLowerLeftLayout()");
            Platform.exit();
        }
    }

    /**
     * The HTMLEditor Layout
     */
    private void initTab4Layout() {
        LOG.debug("Tab 4");
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(RootLayoutTabController.class
                    .getResource("/fxml/FishFXHTMLLayout.fxml"));
            AnchorPane htmlView = (AnchorPane) loader.load();

            // Give the controller the data object.
            fishFXHTMLController = loader.getController();
            fishFXHTMLController.setFishDAO(fishDAO);

            tab_4.setContent(htmlView);
        } catch (IOException ex) {
            LOG.error("initLowerRightLayout error", ex);
            errorAlert("initLowerRightLayout()");
            Platform.exit();
        }
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(resources.getString("sqlError"));
        dialog.setHeaderText(resources.getString("sqlError"));
        dialog.setContentText(resources.getString(msg));
        dialog.show();
    }

}
